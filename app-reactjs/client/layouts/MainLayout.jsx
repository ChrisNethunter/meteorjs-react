import React from 'react';
import AccountsUI from '../../imports/ui/AccountsUI.jsx'

export const MainLayout = ({content}) => (

  <div className="main-layout">
    <header className="docs nav" style={{'backgroundColor':'#001f3f' , 'boxShadow': '0 5px 2px 0 rgba(0,0,0,0.41)'}}>
        <div className="container">
            <div className="row-fluid">
                <div className="brand animated bounceInLeft">
                    <a href="/home">PaperBlog</a>
                </div>
                <nav className="right-float animated bounceInRight mobile-hidden">
                    <a href="/home">Home</a>
                    <a href="/about">About</a>
                    <a href="/categories">Categories</a>
                    <a href="/login">
                        <AccountsUI />
                    </a>

                </nav>
                <div id="hamburger" className="nav-toggle show-mobile animated bounceInRight">
                    <span />
                    <span />
                    <span />
                </div>
            </div>
        </div>
    </header>
    <div className="feature-section container">

        <br></br>
        <br></br>
        {content}
    
    </div>


    <footer>
          <div className="footer-logo text-center">
            <span>S</span>
          </div>
          <p className="text-center">This site was designed and built by <a href="#">PaperBlog</a>,
            Schema is licensed under an <a href="http://opensource.org/licenses/MIT">MIT License</a>.
          </p>
          <ul>

            <li><a href="getstarted.html">Get Started</a></li>
            <li className="font-sm b-c-eee mobile-hidden">|</li>
            <li><a href="documentation.html">Docs</a></li>
            <li className="font-sm b-c-eee mobile-hidden">|</li>
            <li><a href="https://github.com/danmalarkey/schema/issues">Issues</a></li>
            <li className="font-sm b-c-eee mobile-hidden">|</li>
            <li><a href="https://github.com/danmalarkey/schema">Github</a></li>
            <li className="font-sm b-c-eee mobile-hidden">|</li>
            <li><a href="https://github.com/danmalarkey/schema/releases">Releases</a></li>
            <li className="font-sm b-c-eee mobile-hidden">|</li>
            <li><a href="https://github.com/danmalarkey/schema/blob/master/README.md">Changelog</a></li>
          </ul>
          <div className="container icons text-center">
            <nav>
              <a href="https://github.com/danmalarkey/schema"><i className="fa fa-github" /></a>
              <a href="https://twitter.com/schemaui"><i className="fa fa-twitter" /></a>
            </nav>
          </div>
    </footer>
  </div>

)
