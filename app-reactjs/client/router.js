import React from 'react';
import {mount} from 'react-mounter'
import {MainLayout} from './layouts/MainLayout.jsx'
import ResolutionWrapper from '../imports/ui/ResolutionWrapper.jsx'
import ResolutionDetail from '../imports/ui/ResolutionDetail.jsx'
import Login from '../imports/ui/account/Login.jsx'
import About from '../imports/ui/about/About.jsx'
import Categories from '../imports/ui/categories/Categories.jsx'



FlowRouter.route('/',{
	action(){
    //react mounter
    mount(MainLayout,{
        content: (<ResolutionWrapper />)
    })
	}
});

FlowRouter.route('/home',{
	name:'home',
	action(){
    //react mounter
    mount(MainLayout,{
        content: (<ResolutionWrapper />)
    })
	}
});

FlowRouter.route('/about',{
	name:'about',
	action(){
    //react mounter
    mount(MainLayout,{
        content: (<About />)
    })
	}
});

FlowRouter.route('/categories',{
	name:'categories',
	action(){
    //react mounter
    mount(MainLayout,{
        content: (<Categories />)
    })
	}
});



FlowRouter.route('/login',{
	name:'login',
	action(){
    //react mounter
    mount(MainLayout,{
        content: (<Login />)
    })
	}
});

FlowRouter.route('/resolution/:id', {
	name:'resolution-detail',
  action: function(params) {
		//react mounter
		mount(MainLayout,{
				content: (<ResolutionDetail id={params.id} />)
		})
  },

});
