Meteor.methods({

  addResolution : (resolution) => {
    check(resolution, String);

    if (! Meteor.userId()) {
	      throw new Meteor.Error("not-authorized");
	  }
    Resolutions.insert({
                        text:resolution,
                        complete:false,
                        createAt: new Date(),
                        user:Meteor.userId()
                      });
  },

  toogleResolution : (resolution) => {
    check(resolution, Object);
    if (Meteor.userId() !== resolution.user) {
	      throw new Meteor.Error("not-authorized");
	  }
    Resolutions.update({_id:resolution._id},{
                                  $set:{ complete : resolution.complete }
                      });
  },

  deleteResolution : (resolution) => {
    check(resolution, Object);
    if (Meteor.userId() !== resolution.user) {
	      throw new Meteor.Error("not-authorized");
	  }
    Resolutions.remove({_id:resolution._id})
  },

  'createUserWithRole': (data)  =>{

	    const role = ['blogUser'];
	    data.profile.status = true;
      
    	var id = Accounts.createUser({
								      email: data.email,
								      password: data.password,
								      profile: data.profile
								    });
    	Roles.addUsersToRoles(id, role);
    	return id

	},

  'sendEmailVerification': (id)  =>{
		//console.log("id registro " + id)
		Accounts.sendVerificationEmail( id )
	},

});
