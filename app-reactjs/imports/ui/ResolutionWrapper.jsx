import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

import ResolutionsForm from './ResolutionsForm.jsx'
import ResolutionsSingle from './ResolutionsSingle.jsx'

// App component - represents the whole app
//export default class App extends React.Component {
export default class ResolutionWrapper extends TrackerReact(React.Component) {
  constructor(){
    super();
    this.state = {
      //subscription works thanks to tracker react
      subscription:{
        resolutions:Meteor.subscribe("userResolutions")
      }
    };
  }
  componentWillUnmount(){
    this.state.subscription.resolutions.stop();
  }
  resolutions(){
    return Resolutions.find().fetch();
  }
  render() {
    //let res = this.resolutions();

    /*if (res.length < 1 ) {
        return (<div>Loading</div>);
        //console.log(res[0]._id);
    }else{
      console.log(res[0]._id);
    }*/
    /*for (var i=0; i < Object.keys(res).length; i++){
      console.log(res[i]._id);
    }*/

    return (
      <div>
        <h3>My React Form</h3>
        <br></br>

        {/*Llamando un Componente*/}
        <ResolutionsForm />

        <table className="table bordered">
          <thead>
            <tr>
                <th>Status</th>
                <th>Name</th>
                <th>Options</th>
            </tr>
          </thead>
          <tbody>

             {
               this.resolutions().map( (resolution) => {
                 return <ResolutionsSingle key={resolution._id} resolution={resolution}/>
               })
             }

          </tbody>
        </table>

      </div>
    );
  }
}
