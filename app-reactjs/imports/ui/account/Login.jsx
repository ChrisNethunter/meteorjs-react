import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import SingUp from './SingUp.jsx'
import { Session } from 'meteor/session';

export default class Login extends TrackerReact(React.Component){
  constructor(props) {
    super(props)
    this.state = {
      statusLogin: "",
      statusSingUp: "",
      messageForm:"",
      classMessage:""
    };
    Session.set("dataMessage");
  }
  componentWillMount (){

  }
  componentDidMount() {

  }
  accountsLoginMeteor(event){
    event.preventDefault();
    let email = this.refs.email.value.trim();
    let password = this.refs.password.value.trim();
    document.getElementById("message").innerHTML = "";

    Meteor.loginWithPassword(email, password, function(err,res){
        if (err){
          if (err.message === 'User not found [403]') {

            document.getElementById("message").textContent += "User not found";

            //$("#message").append("User not found");
            /*Session.set("dataMessage",{
              messageForm:"User not found",
              classMessage:"a-is-danger"}
            );*/

          }else{
            document.getElementById("message").textContent += "User or Password incorrect";
            //$("#message").append("User or Password incorrect");
            /*Session.set("dataMessage",{
                                        messageForm:"User or Password incorrect" ,
                                        classMessage:"a-is-danger"
                        });*/
          }
        }else{
          FlowRouter.go('home');
        }
    });


    /*if (Session.get("dataMessage")) {
      this.setState({
                        messageForm:Session.get("dataMessage").messageForm,
                        classMessage: Session.get("dataMessage").classMessage
                      });
    }else{
      console.log("hiii")
    }*/
  }
  showSingUp(event){
    event.preventDefault();
    let login = document.getElementById("login");
    login.classList.add("ng-hide-form");

    let singUp = document.getElementById("singup");
    singUp.classList.add("ng-show-form");
  }
  handleShowForm(event) {
    event.preventDefault();
    if (this.state.statusLogin != "") {
      return this.setState({
                            statusLogin: "",
                            statusSingUp: ""
                          });
    }
    return this.setState({
                          statusLogin: "ng-hide-form",
                          statusSingUp: "ng-show-form"
                        });
  }

  render(){
    if (Meteor.userId()) {
      return (<h1>Profile</h1>)
    }
    return(
      <div className="row-fluid">
        <div className="col4">
          <div id="login" className={this.state.statusLogin}>
            <div className="panel panel-default nt-panel">
               <div className="panel-title">
                 <h3>Iniciar Sesión</h3>
               </div>
               <div className="panel-body">
                 <form  onSubmit={this.accountsLoginMeteor.bind(this)}>
                   <div className="form-group">
                     <label>Correo</label>
                     <input type="email"
                       ref="email"
                       className="form-element nt-blog-inputs"
                       placeholder="email@email.com"
                       required={true}/>
                   </div>
                   <div className="form-group">
                     <label>Contraseña</label>
                     <input type="password"
                            ref="password"
                            className="form-element nt-blog-inputs"
                            placeholder="Password"
                            required={true}
                            />
                    </div>
                    <p id="message" align="center" className="a-is-danger" ref="message"  style={{marginBottom: 15}} ></p>
                    <div className="form-group">
                      <button type="submit" align="center" className="btn btn-primary btn-nt-blogs"><span>Iniciar Seción</span></button>
                    </div>
                 </form>

                 <div className="form-group" align="center">
                   <a href="#" onClick={this.handleShowForm.bind(this)}>Regístrate</a>
                 </div>
               </div>
              </div>
          </div>
          <div id="singup" style={{display: 'none'}} className={this.state.statusSingUp}>
            <SingUp states={this.state} handleShowFormOpt={this.handleShowForm.bind(this)}/>
          </div>
        </div>
      </div>
    )
  }
}
