import React, { Component } from 'react';
export default class SingUp extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      classValidate: "",
      messageClass:""
    };
  }

  accountsSingUpMeteor(event){
    event.preventDefault();

    document.getElementById("messageSingUp").innerHTML = "";
    let ready            = false;
    let confirmPassword  = this.refs.confirmpassword.value.trim();
    document.getElementById("messageSingUp").className = "";
    let user =  {
                    email     : this.refs.email.value.trim(),
                    password  : this.refs.password.value.trim(),
                    profile   : {
                                  name      : this.refs.name.value.trim(),
                                }
                };

    if (validateEmail(user.email) &&  isNotEmpty(user.profile.name) == true) {
      ready = true;
    }

    console.log(confirmPassword + " -- " + user.password);

    if (confirmPassword == user.password && ready == true ) {
      ready = true;
    }else{
      ready = false;
      this.setState({ classValidate: "ng-error-input" });
    }

    if (ready == true ) {

      Meteor.call('createUserWithRole',user,(err, res) => {
        if (!err){
          if(res.length){
            document.getElementById("messageSingUp").className = "alert a-is-success";
            document.getElementById("messageSingUp").textContent += "Bien echo! Se ha registrado exitosamente. Consulta tu correo electrónico";
            /*Meteor.call('sendEmailVerification',res,(err, resp) => {
              if (err){
                console.log(err)
              }
            });*/
          }
        }else{
          let message;
          document.getElementById("messageSingUp").className = "a-is-danger";
          if (err.reason == "Email already exists.") {
              message = "Este correo ya esta registrado";
          }else{
            message = err.reason;
          }
          document.getElementById("messageSingUp").textContent += message ;
        }
      });
    }else{

      document.getElementById("messageSingUp").textContent += "Porfavor Ingresa la informacion correcta!";
    }
  }
  render(){
    //console.log(this.props.states.statusLogin + " desde singup")
    return(
        <div className="panel panel-default nt-panel">
          <div className="panel-title">
            <h3>Regístrate</h3>
          </div>
         <div className="panel-body">
           <form  onSubmit={this.accountsSingUpMeteor.bind(this)}>
             <div className="form-group">
               <label>Nombre Completo *</label>
               <input type="text"
                      ref="name"
                      className="form-element nt-blog-inputs"
                      placeholder="Name"
                      />
             </div>
             <div className="form-group">
               <label>Correo *</label>
               <input type="email"
                      ref="email"
                      className="form-element nt-blog-inputs"
                      placeholder="email@email.com"/>
             </div>
             <div className="form-group">
               <label>Contraseña *</label>
               <input type="password"
                      ref="password"
                      className={"form-element nt-blog-inputs " + this.state.classValidate}
                      placeholder="Password"
                      required={true} />
             </div>
             <div className="form-group">
               <label>Confirmar Contraseña *</label>
               <input type="password"
                      ref="confirmpassword"
                      className={"form-element nt-blog-inputs " + this.state.classValidate}
                      placeholder="Password"
                      required={true} />
             </div>

             <p id="messageSingUp"
                align="center"
                className={this.state.messageClass}
                style={{marginBottom: 15}}></p>

             <div className="form-group">
               <button  type="submit"
                        align="center"
                        className="btn btn-primary btn-nt-blogs"
                        >
                        <span>Registrarse</span>
              </button>
             </div>

             <div className="form-group" align="center">
              <a href="#" onClick={this.props.handleShowFormOpt}>Iniciar Sesión</a>
              </div>
           </form>
         </div>
       </div>
    )
  }
}
