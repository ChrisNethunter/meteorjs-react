import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';
import { Meteor } from 'meteor/meteor';

export default class ResolutionDetail extends TrackerReact(React.Component) {
  constructor(){
    super();
    this.state = {
      //subscription works thanks to tracker react
      subscription:{
        resolutions:Meteor.subscribe("allResolutions")
      }
    };
  }
  componentWillUnmount(){
    this.state.subscription.resolutions.stop();
  }
  resolution(){
    return Resolutions.findOne(this.props.id);
  }
  render(){
    let res = this.resolution();
    if (!res) {
      return (<div>Loading...</div>);
    }
    return (
      <div>
        <h3>Resolution Detail {res.text}</h3>
      </div>
    )



  }
}
