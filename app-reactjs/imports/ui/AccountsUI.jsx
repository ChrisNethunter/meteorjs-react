import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class AccountsUI extends TrackerReact(React.Component) {
  componentDidMount() {
    /*this.view = Blaze.render(Template.loginButtons ,
                             ReactDOM.findDOMNode(this.refs.container)
                            );*/
  }
  componentWillUnmount () {
    //Blaze.remove(this.view);
  }
  render(){
    if (Meteor.userId()) {
      let data = Meteor.user();
      if (data) {
        //console.log("is login " + data.emails[0].address)
        return (<span>{data.emails[0].address}</span>)
      }
    }

    return(<span>Login/SignUp</span>)
    //return <span ref="container" />
  }

}
