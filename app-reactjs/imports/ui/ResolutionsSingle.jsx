import React, {Component} from 'react';

export default class ResolutionsSingle extends Component{
  toggleChecked (){
    //console.log(this.props.resolution.complete)
    Meteor.call("toogleResolution",this.props.resolution,(error, result) => {
      if (!error) {
        //this.refs.resolution.value = "";
      }else{
        console.log("error " + error)
      }
    });
  }
  deleteResolution(){
    Meteor.call("deleteResolution", this.props.resolution ,(error, result) => {
      if (!error) {
        //this.refs.resolution.value = "";
      }else{
        console.log("error " + error)
      }
    });
  }
  render (){
    const classStatusResolution = this.props.resolution.complete ? "checked" : "";
    return(
      <tr>
         <td>
           <input
               type="checkbox"
               readOnly={true}
               checked={this.props.resolution.complete}
               onClick={this.toggleChecked.bind(this)}
               style={{opacity: '1', pointerEvents: 'auto'}}
           />
         </td>
           <td className={classStatusResolution}>
           <a href={`/resolution/${this.props.resolution._id}`}>{this.props.resolution.text}</a>
         </td>
         <td>
           <a className="btn btn-danger-outline"
              onClick={this.deleteResolution.bind(this)}>
             Remove
           </a>
         </td>
      </tr>
    )
  }
}
