import React, { Component } from 'react';
import TrackerReact from 'meteor/ultimatejs:tracker-react';

export default class About extends TrackerReact(React.Component){
  render(){
    return(
      <section className="feature-section">
        <div className="container">
          <h1 className="feature-section-title">From prototype to production</h1>
          <p className="feature-lead text-center">Whether you’re trying to put a quick design to a prototype, or if you’re working on a
            production-ready application, Schema provides the foundation and components to easily design any
            responsive web project.
          </p>
          <div className="row-fluid">
            <div className="col4 fade first animated fadeInDown animate">
              <div className="text-center">
                <img src="http://danmalarkey.github.io/schema/src/img/devices.svg" alt />
              </div>
              <h2 className="text-center">Responsive</h2>
              <p>Schema comes fully equipped with the capabilities of creating
                familiar experiences across multiple viewports. From a desktop
                monitor down to a mobile device, Schema's 12-column grid provides flexibility.
              </p>
            </div>
            <div className="col4 second fade animated fadeInDown animate">
              <div className="text-center">
                <img src="http://danmalarkey.github.io/schema/src/img/less_logo.svg" className="less-logo" alt />
              </div>
              <h2 className="text-center">Less CSS</h2>
              <p>Schema leverages the power of <a href="http://lesscss.org">Less</a>. This enables a clean
                structure of code that is super easy to maintain.
                Need something to compile your Less, and on a Mac? I totally recommend <a href="http://incident57.com/codekit">Codekit</a>. Seriously.
              </p>
            </div>
            <div className="col4 third fade animated fadeInDown animate">
              <div className="text-center">
                <img src="http://danmalarkey.github.io/schema/src/img/contribute.svg" alt />
              </div>
              <h2 className="text-center">Contribute</h2>
              <p>Want to help us make Schema even better? Schema is open source, so if you want to take part
                in contributing <a href="https://github.com/danmalarkey/schema">please fork the repo on Github</a> and send us a pull request. Or, if you find a bug <a href="https://github.com/danmalarkey/schema/issues">create a new issue</a>.
              </p>
            </div>
          </div>
        </div>
      </section>
    )
  }
}
