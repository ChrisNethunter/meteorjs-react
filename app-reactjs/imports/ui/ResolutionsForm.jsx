import React, {Component} from 'react';

export default class ResolutionsForm extends Component{
  addResolution(event){
    event.preventDefault();

    var text = this.refs.resolution.value.trim();

    if (text) {
      Meteor.call("addResolution",text ,(error, result) => {
        if (!error) {
          this.refs.resolution.value = "";
        }else{
          Bert.alert('Please login before submitting' , 'danger' ,'fixed-top', 'fa-frown-o');
        }
      });
    }
  }
  render(){
    return(
      <form className="new-resolution" onSubmit={this.addResolution.bind(this)}>

          <input id="last_name" type="text" ref="resolution" placeholder="Finish React Meteor Series" className="validate nt-blog-inputs"/>

      </form>
    )
  }
}
